#!/bin/bash

if [ -z "$HOST" ]
then
  HOST="localhost"
fi

echo "Testing Service-Info"
curl --fail http://$HOST:$HOST_PORT/info/service || exit 1

echo "Testing Healthcheck"
curl --fail http://$HOST:$HOST_PORT/info/healthcheck || exit 1