#!/bin/bash

NAME="api"
INSTANCES=$1

if [ -z "$INSTANCES" ]
then
  echo "Number of instances not supplied"
else
  export DEBUG=app:* # Enabling Debugging Logs

  webpack --mode=production --config=./webpack.config.js
  pm2 delete $NAME
  pm2 start ./dist/index.js --name=$NAME --instances $INSTANCES
fi
