#!/bin/bash

TERMS=("webpack" "pm2")

for term in "${TERMS[@]}"
do
  echo "Terminating: $term"

  # Get all processes -> Find using a term -> Skip the first line (Grep itself) -> Get Second column (PID)
  processes=$(ps aux | grep $term | sed 1,1d | awk -F ' ' '{print $2}')

  for process in "${processes[@]}"
  do
    [ ! -z "$process" ] && { kill -9 $process; echo "Killed: $process"; }
  done
done
