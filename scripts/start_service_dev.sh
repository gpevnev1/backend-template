#!/bin/bash

NAME="api:dev"

export DEBUG=app:* # Enabling Debugging Logs

echo "Processes started" > ./scripts/run

pm2 delete $NAME

# IMPORTANT: NO "nohup" (Will NOT terminate even when console is destroyed - Too much)
command webpack --config=./webpack.config.js --watch --mode=development &
webpack_process=$!

# command pm2 start ./dist/index.js --no-daemon --watch --name=$NAME --instances=1 &
# echo $! >> ./scripts/run

# Watching file for changes + Not daemon (Logs / Fixed) -> NOT BACKGROUND
pm2 start ./dist/index.js --no-daemon --watch --name=$NAME --instances=1
# NOTE: Does NOT terminate with console (Resumes - The purpose of Process-Runner)

# Cleaning up webpack (Optional)
#kill -9 $webpack_process