#!/bin/bash

[ -z "$DOCKER_IMAGE" ] && { echo "No Image-Name"; exit 1; }

if [ -z "$DOCKER_ACCESS_TOKEN" ] || [ -z "$DOCKER_USERNAME" ]
then
  echo "Docker credentials not provided"
  exit 1
fi

if [ -z "$DOCKER_REGISTRY" ]
then
  DOCKER_REGISTRY="hub.docker.com/"
fi

REMOTE_DOCKER_IMAGE="$DOCKER_REGISTRY/$DOCKER_USERNAME/$DOCKER_GROUP/$DOCKER_IMAGE"

echo "Remote-Image: $REMOTE_DOCKER_IMAGE"

docker logout
echo "$DOCKER_ACCESS_TOKEN" | docker login $DOCKER_REGISTRY --username=$DOCKER_USERNAME --password-stdin

docker pull $REMOTE_DOCKER_IMAGE
docker image tag $REMOTE_DOCKER_IMAGE $DOCKER_IMAGE