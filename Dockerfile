# Installing development dependencies
FROM node AS dependencies

WORKDIR /build

# IMPORTANT: Installing dependencies BEFORE copying files (NOT re-installing unless dependencies have changed)
# (Otherwise reinstallation will be getting performed every timer the source / configuration files change)
COPY ./package.json /build/package.json
RUN npm install
# Note: Cannot ONLY install dev-dependencies

# Building output files
FROM dependencies AS build

ENV NODE_ENV=production

COPY ./.babelrc /build/.babelrc
COPY ./webpack.config.js /build/webpack.config.js
COPY ./src/ /build/src/

RUN npm run build

# Running actual application
FROM node:alpine

WORKDIR /app

# Installing Globals (That running / compilation / checking depends on)
# Process-Runner
RUN npm install -g pm2
# Health-Checking Client
RUN apk add curl


# IMPORTANT: Installing dependencies BEFORE copying files (NOT re-installing unless dependencies have changed)
# (Otherwise reinstallation will be getting performed every timer the source / configuration files change)
COPY ./package.json /app/package.json
RUN npm install --only=production
# Note: ONLY installing Production-Dependencies (NO Dev-Dependencies)


# IMPORTANT: Postpone copying Scripts / Configuration / Assets / Static files that do NOT affect Compilation
#     - Do NOT add unless actually used properly
COPY --from=build /build/dist/index.js /app/dist/index.js
COPY ./config/ /app/config/
# SUPER-IMPORTANT: "config" is loaded at runtime -> Can ONLY use Native Node.js ("module.exports")
COPY ./ecosystem.config.js /app/ecosystem.config.js


# Enabling logger
ENV DEBUG="app:*"
# Note: Providing Default Environment-Variables (Debugging / Running / etc.)

HEALTHCHECK --interval=10s --timeout=10s --start-period=15s --retries=3 CMD curl --fail http://$SERVICE_HOST:$SERVICE_PORT/info/healthcheck || exit 1

# Note: Can only be written and specified in the following way (Each separate piece of command as an array element)
CMD ["pm2", "start", "--no-daemon", "ecosystem.config.js", "--env=production"]