module.exports = {
  service: {
    name: "Service-Name",
    port: 3000,
    host: "127.0.0.1"
  },

  domain: {},

  details: {
    mongodb: {
      url: "mongodb://localhost:27017/test"
    },
    postgres: {
      url: "postgresql://localhost:5432/test"
    },
    redis: {
      url: "redis://localhost:6379"
    }
  }
};