const PORT = Number(process.env.SERVICE_PORT);
const HOST = String(process.env.SERVICE_HOST);

module.exports = {
  service: {
    port: PORT,
    host: HOST
  },
};