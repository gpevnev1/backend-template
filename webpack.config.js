const path = require("path");
const externals = require("webpack-node-externals");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

const SRC = "./src/index.js";
const DIST = "dist";
const ENV = String(process.env.NODE_ENV || "production");

module.exports = {
  entry: {
    index: SRC
  },

  output: {
    path: path.join(__dirname, DIST),
    filename: "[name].js"
  },

  resolve: {
    extensions: [".js"]
  },

  // Using Bable for compilation
  module: {
    rules: [
      {
        test: /^\.js$/,
        exclude: [/node_modules/],
        use: "babel-loader"
      }
    ]
  },

  // Cleaning up (Production Only - Disrupts Development Workflow)
  plugins:
    ENV === "production"
      ? [new CleanWebpackPlugin({
        cleanOnceBeforeBuildPatterns: path.join(__dirname, DIST)
      })] // Clean up directory in production
      : [],

  target: "node", // Using Node (External Libraries, Filenames, Paths, Native Libraries. etc.)

  mode: ENV, // Development / Production Mode

  // Using external libraries (NOT compiling directly)
  externals: externals(),

  // NOT keeping "__dirname" and "__filename" (Setting to the NEW location) - CORRECT behavior
  node: {
    __dirname: false, // "../dist"
    __filename: false // "../dist/index.js"
  }
}