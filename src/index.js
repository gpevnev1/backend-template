import config from "config";
import debug from "debug";
import { createApi } from "./api";

const logger = debug("app");
const apiLogger = logger.extend("api");

const api = createApi({
  service: config.get("service"),

  logger: apiLogger
});

api.start();