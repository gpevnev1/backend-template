import express from "express";
import parser from "body-parser";
import cors from "cors";

export const createServer = ({ }) => {
  const app = express();

  app.use(parser.json());
  app.use(parser.urlencoded({ extended: false }));

  app.use(cors());

  return app;
};