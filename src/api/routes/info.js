import { Router } from "express";
import os from "os";

export const createInfoRouter = ({
  service: {
    name
  },

  logger
}) => {
  const infoRouter = Router();

  infoRouter.route("/service").get((req, res) => {
    logger("Info");

    return res.status(200).json({
      service: {
        name
      },
      instance: {
        host: os.hostname(),
        id: process.pid
      },
      at: Date.now()
    });
  });

  infoRouter.route("/healthcheck").get((req, res) => {
    logger("Healtcheck performed");

    return res.status(200).json({
      instance: {
        host: os.hostname(),
        id: process.pid
      },
      at: Date.now()
    });
  });

  return infoRouter;
};
