import { createInfoRouter } from "./routes/info";
import { createServer } from "./server";

export const createApi = ({
  service: {
    name,
    port,
    host
  },

  logger
}) => {
  const server = createServer({});

  server.use("/info", createInfoRouter({
    service: { name },
    logger
  }));

  return {
    start() {
      server.listen(port, host, () => {
        logger(`Service started on ${host}:${port} at ${Date.now()}`)
      })
    },
    stop() {
      server.close();
    }
  };
};
